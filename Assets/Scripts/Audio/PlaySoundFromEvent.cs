﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundFromEvent : MonoBehaviour
{
    public string sound ="";
    public bool playOnAwake;
    private void Start()
    {
        if (playOnAwake)
        {
            PlayEvent();
        }

    }
    public void PlayEvent()
    {
        //Debug.Log("Calling");
        AkSoundEngine.PostEvent(sound, gameObject);
    }
}
