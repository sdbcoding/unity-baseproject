﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceTowardsVector3Variable : OnUpdate
{
    public Vector3Variable facingdirectionNormalized;

    Vector3 newFacingDirection;
    public override void UpdateEventRaised(float deltaTime)
    {
        newFacingDirection = new Vector3(facingdirectionNormalized.Value.x, 0, facingdirectionNormalized.Value.y);
        transform.LookAt(transform.position + newFacingDirection);
    }
}
