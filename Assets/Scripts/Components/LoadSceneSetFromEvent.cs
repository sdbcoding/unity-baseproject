﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class LoadSceneSetFromEvent : MonoBehaviour {

	public SceneSet scene;
	public UnityEvent response; 
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	public void LoadSceneSet() 
	{
		StartCoroutine( loadScene());
		/* 
		if (scene.scenePaths.Length != 0)
		{
			SceneManager.LoadScene( scene.scenePaths[0], LoadSceneMode.Single);
			for (int i = 1; i < scene.scenePaths.Length; i++)
			{
				SceneManager.LoadScene( scene.scenePaths[i], LoadSceneMode.Additive);
			}
		}
		*/
	}
	IEnumerator loadScene()
	{
		//Debug.Log("Starting");
		yield return new WaitForEndOfFrame();
		//Debug.Log("Done yeild");

		response.Invoke();
		if (scene.scenePaths.Length != 0)
		{
			SceneManager.LoadScene( scene.scenePaths[0], LoadSceneMode.Single);
			for (int i = 1; i < scene.scenePaths.Length; i++)
			{
				SceneManager.LoadScene( scene.scenePaths[i], LoadSceneMode.Additive);
			}
		}
	}

}
