﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpdateTextFromFloatValue : GameEventListener {
	Text textComponent;
	public FloatReference points;
	
	// Use this for initialization
	void Start () {
		textComponent = GetComponent<Text>();
		if (!points.UseConstant)
		{
			Event = points.Variable;
			Event.RegisterListener(this);
			Response.AddListener(UpdateText);
		}
		UpdateText();
	}

	public void UpdateText()
	{
		textComponent.text = points.Value.ToString();
	}

	
	
}
