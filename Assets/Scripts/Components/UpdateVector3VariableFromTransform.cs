﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateVector3VariableFromTransform : MonoBehaviour
{
    public Vector3Variable playerPosition;
    

    void Update()
    {
        playerPosition.SetValue(transform.position);
    }
}
