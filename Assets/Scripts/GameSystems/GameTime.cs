﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTime : OnUpdate
{
    public static float ElapsedTime;
    

    public override void UpdateEventRaised(float deltaTime)
    {
        ElapsedTime += deltaTime;
    }
}
