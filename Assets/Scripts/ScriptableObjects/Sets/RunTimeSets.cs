﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public abstract class RunTimeSets<T> : GameEvent {
    
	
	public List<T> Items = new List<T>();

	public void Add(T t)
	{
		if (!Items.Contains(t)) 
		{
			Items.Add(t);
			Raise();
		}
	}

	public void Remove(T t)
	{
		if (Items.Contains(t)) 
		{
			Items.Remove(t);
			Raise();
		}
	}

	public List<T> GetItems()
	{
		CheckItemsForNull();
		return Items;
	}
	public T GetItemsAtIndex(int index)
	{
		return Items[index];
	}
	public void SetItemAtIndex(int index,T value)
	{
		Items[index] = value;
	}
	public void SetItems(List<T> newItems)
	{
		Items = newItems;
	}
    public int GetItemListSize()
    {
        CheckItemsForNull();
        return Items.Count;
    }

	void CheckItemsForNull()
	{
		for (int i = Items.Count - 1; i >=0 ; i--)
        {
            if (Items[i] == null || Items[i].ToString() == "null")
            {
                Items.RemoveAt(i);
            }
        }
	}
	
	
}
