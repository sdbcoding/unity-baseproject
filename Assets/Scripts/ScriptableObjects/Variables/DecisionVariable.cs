﻿
using UnityEngine;

[CreateAssetMenu(fileName = "Decision", menuName = "Variables/Decision Variable", order = 8)]
public class DecisionVariable : GameEvent
{
    public Decision Value;


    public void SetValue(Decision value)
    {
        Value = value;
        Raise();

    }

    public void SetValue(DecisionVariable value)
    {
        Value = value.Value;
        Raise();

    }
    public void ClearValue()
    {
        Value = null;
        Raise();

    }
}
