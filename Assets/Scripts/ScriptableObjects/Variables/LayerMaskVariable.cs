﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Variables", menuName = "Variables/LayerMask Variable", order = 7)]

public class LayerMaskVariable : GameEvent
{
    [SerializeField]
    private LayerMask Value;

    public void SetValue(LayerMask layerMask)
    {
        Value = layerMask;
        Raise();
    }

    public void SetValue(LayerMaskVariable layerMask)
    {
        Value = layerMask.Value;
        Raise();

    }

    public LayerMask GetValue()
    {
        return Value;
    }

    public void ApplyChange(LayerMask layerMask)
    {
        Value += layerMask;
        Raise();
    }

    public void ApplyChange(LayerMaskVariable layerMask)
    {
        Value += layerMask.Value;
        Raise();
    }
}
