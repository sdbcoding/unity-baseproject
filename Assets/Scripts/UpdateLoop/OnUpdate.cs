﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class OnUpdate : MonoBehaviour {
	public UpdateEvent updateEvent;

	public virtual void UpdateEventRaised(float deltaTime)
	{

	}
    
    protected virtual void OnEnable()
    {
        if (updateEvent != null)
            updateEvent.RegisterListener(this);
    }
    private void OnDisable()
    {
        if (updateEvent != null)
            updateEvent.UnRegisterListener(this);
    }
}
	

